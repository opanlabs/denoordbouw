<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Denoordbouw</title>
	<link rel="stylesheet" href="{{ asset('/app/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/app/css/slick.css') }}">
	<link rel="stylesheet" href="{{ asset('/app/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/app/css/print.css') }}">
	
	<script src="{{ asset('/app/js/jquery-1.9.1.min.js') }}"></script>
	<script src="{{ asset('/app/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('/app/js/slick.min.js') }}"></script>
	
	<script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBEJKMpH6lYi7wUxzwVyslbr4y85Aq7WVw&libraries=geometry&callback=initMap">
	</script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script>
		var page = '{{$page->id}}';
	</script>
	<script src="{{ asset('/app/js/mainNew.js') }}"></script>
</head>
<body itemscope itemtype="http://schema.org/WebPage">
    <!-- Google Analytics -->
    @if (!empty($settings['ga_tracking_id']->value))
        @include('partials.tracking.google-analytics', ['trackingId' => $settings['ga_tracking_id']->value])
    @endif
	
	<div id="wrapper">
	@yield('content')
    @include('common.footer')

</body>
</html>