@extends('layouts.app')

@section('title', 'Pagina niet gevonden')

@section('content')
	 <div id="page">
			<div class="header">
				<div class="container">
					<div class="row">
						<div class="header-left col-md-6 col-xs-12">
							<div class="logo"><a href="#"><img src="{{ asset('/app/img/logo.png') }}" alt="" class="img-responsive"></a></div>
							<div class="navbar-header visible-xs">
					          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					            <span class="icon-bar"></span>
					            <span class="icon-bar"></span>
					            <span class="icon-bar"></span>
					          </button>
					        </div>
						</div>
						<div class="header-right col-md-6 col-xs-12">
							<div class="header-right-wrapper">
								<div class="header-right-1 hidden-xs">
									<ul>
										<li><a href="tel:{{ $settings['phone']->value }}">{{ $settings['phone']->value }} </a></li>
										<li><a href="mailto:{{ $settings['email']->value }}">{{ $settings['email']->value }}</a></li>
									</ul>
								</div>
								<div class="header-right-2">
									<div class="menus-desktop hidden-xs">
										<ul>
												@foreach ($navigation as $key=>$row)
											
												<li><a class="active" href="{{url($row['slug'])}}">{{$row['title']}}</a>
												
												</li>
											@endforeach
										</ul>
									</div>
									<div class="menus-mobile visible-xs">
										
										<div id="navbar" class="navbar-collapse collapse">
								          <ul class="nav navbar-nav">
								           	@foreach ($navigation as $key=>$row)
											
												<li><a class="active" href="{{url($row['slug'])}}">{{$row['title']}}</a>
												@if(count($row['subpages'])>0)
													<ul class="submenu">
														
															@foreach ($row['subpages'] as $keysub=>$rowsub)
															<li><a href="{{$rowsub['slug']}}">{{$rowsub['title']}}</a></li>
														@endforeach
														
													</ul>
												@endif
												</li>
											@endforeach
								          </ul>
								        </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="page-bg-wrapper" >
				<div class="overlay"></div>
				<div class="content-home">
					<div class="container">
						<div class="tagline">
							<h3> No Page</h3>
							
						</div>
					</div>
				</div>
			</div>
			<div class="inner-detail-wrapper">
				<div class="container">
					<div class="row">
						<div class="column-left col-md-4">
							<div class="widget">
								<div class="menus satu">
									<ul>
										@foreach($subPagesNavigation as $key=>$row)
										<?php $images = $row->images()->get()->toArray(); ?>
											<li @if($images!='') style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $images[0]['path'] .'/icon.' . $images[0]['extension']) }})  no-repeat scroll 5% center !important" @endif><a href="{{$row['slug']}}">{{$row['title']}}</a></li>
										@endforeach
									</ul>
								</div>
							</div>
							<div class="widget">
								<div class="title">OUR CONTACT</div>
								<div class="menus dua">
									<ul>
										<li><div class="ico"><i class="fa fa-map-marker" aria-hidden="true"></i></div>{{ $settings['address']->value }}</li>
										<li>
											<a href="tel:{{ $settings['phone']->value }}">
											<div class="ico"><i class="fa fa-phone" aria-hidden="true"></i></div>
											{{ $settings['phone']->value }}</a>
										</li>
										<li>
											<a href="tel:{{ $settings['mobile']->value }}">
											<div class="ico"><i class="fa fa-mobile" aria-hidden="true"></i></div>
											{{ $settings['mobile']->value }}</a>
										</li>
										<li>
											<a href="mailto:{{ $settings['email']->value }}">
											<div class="ico"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
											{{ $settings['email']->value }}</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="column-right col-md-8">
							<div class="content">
								<div class="content-txt">
									 @if ($msg = $settings['message_404']->value)
											
											<p>{{ $msg }}</p>

											<p>&nbsp;</p>
										@else
											Helaas! De pagina die u zoekt kan niet worden gevonden.
										@endif
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
    <div class="title">
       
    </div>
@endsection