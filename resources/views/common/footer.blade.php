<div id="footer">
			<div class="footer-widget">
				<div class="container">
					<div class="row">
						<div class="footer-column-block col-md-4">
							<div class="footer-widget-block">
								<div class="img"><img src="{{ asset('/app/img/logo-bottom.png') }}" alt="" class="img-responsive"></div>
								<div class="desc">{!!(isset($statistic['5']))?strip_tags($statistic['5']['text']):''!!}</div>
							</div>
						</div>
						<div class="footer-column-block col-md-2">
							<div class="footer-widget-block">
								<div class="title">menu</div>
								<div class="list-menu">
									<ul>
										@foreach ($navigation as $key=>$row)

										<li><a href="{{$row['slug']}}">{{$row['title']}}</a></li>
										
										@endforeach

									</ul>
								</div>
							</div>
						</div>
						<div class="footer-column-block col-md-6">
							<div class="footer-widget-block">
								<div class="title">adres</div>
								<div class="desc">
									{!!(isset($statistic['7']))?$statistic['7']->text:''!!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>		
			<div class="footer-bottom">
				<div class="container">
					<div class="txt">
						<div class="pull-left">Copyright © 2017 De_Noordbouw.com All rights reserved.  </div>
						<div class="pull-right">Website door: <a href="https://www.ceesenco.com/" target="_blank" style="color: #181847;text-decoration: none;">Cees & Co</a></div>
					</div>
					<div class="sosmed">
						<ul>
							@if($settings['twitter']['value']!='')
							<li><a href="{{$settings['twitter']['value']}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							@endif
							@if($settings['facebook']['value']!='')
							<li><a href="{{$settings['facebook']['value']}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							@endif
							@if($settings['google']['value']!='')
							<li><a href="{{$settings['twitter']['value']}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							@endif
							@if($settings['pinterest']['value']!='')
							<li><a href="{{$settings['pinterest']['value']}}"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
							@endif
						</ul>
					</div>
				</div>
			</div>	
		</div>