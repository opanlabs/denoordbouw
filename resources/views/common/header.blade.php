<!-- Master Header -->
<div class="header {{($page->id==1)?'transparent':''}}">
				<div class="container">
					<div class="row">
						<div class="header-left col-md-6 coxl-xs-12">
							<div class="logo"><a href="home"><img src="{{ asset('/app/img/logo.png') }}" alt="" class="img-responsive"></a></div>
							<div class="navbar-header visible-xs">
					          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					            <span class="icon-bar"></span>
					            <span class="icon-bar"></span>
					            <span class="icon-bar"></span>
					          </button>
					        </div>
						</div>
						<div class="header-right col-md-6 col-xs-12">
							<div class="header-right-wrapper">
								<div class="header-right-1 hidden-xs">
									<ul>
										<li><a href="tel:{{ $settings['phone']->value }}">{{ $settings['phone']->value }} </a></li>
										<li><a href="mailto:{{ $settings['email']->value }}">{{ $settings['email']->value }}</a></li>
									</ul>
								</div>
								<div class="header-right-2">
									<div class="menus-desktop hidden-xs">
										<ul>
											@foreach ($navigation as $key=>$row)
											
												<li><a class="active" href="{{url($row['slug'])}}">{{$row['title']}}</a>
												@if(count($row['subpages'])>0)
													<ul class="submenu">
														
															@foreach ($row['subpages'] as $keysub=>$rowsub)
															<li><a href="{{$rowsub['slug']}}">{{$rowsub['title']}}</a></li>
														@endforeach
														
													</ul>
												@endif
												</li>
											@endforeach
										</ul>
									</div>
									<div class="menus-mobile visible-xs">
										
										<div id="navbar" class="navbar-collapse collapse">
								          <ul class="nav navbar-nav">
								            @foreach ($navigation as $key=>$row)
											
												<li><a class="active" href="{{url($row['slug'])}}">{{$row['title']}}</a>
												@if(count($row['subpages'])>0)
													<ul class="submenu">
														
															@foreach ($row['subpages'] as $keysub=>$rowsub)
															<li><a href="{{$rowsub['slug']}}">{{$rowsub['title']}}</a></li>
														@endforeach
														
													</ul>
												@endif
												</li>
											@endforeach
												<div class="mobile-second-menu">
									          	<ul>
													<li><a href="tel:{{ $settings['phone']->value }}">{{ $settings['phone']->value }} </a></li>
													<li><a href="mailto:{{ $settings['email']->value }}">{{ $settings['email']->value }}</a></li>
												</ul>
									          </div>
								          </ul>
								        </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>