@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
    <div id="page">
			@include('common.header')
			<?php $images = $page->images()->get()->toArray(); ?>
			<div class="page-bg-wrapper" @if($images!='') style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}) no-repeat scroll center center / cover !important" @endif >
				<div class="overlay"></div>
				<div class="content-home">
					<div class="container">
						<div class="tagline">
							<h3>{!!$page->intro!!}</h3>
							<h2>{!!$page->text!!}</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="contact-info-wrapper">
				<div class="container">
					@if(isset($statistic['12']))
					<div class="title">{!!$statistic['12']['title']!!}</div>
					<div class="desc">
						{!!$statistic['12']['text']!!}
					</div>
					@endif
					<div class="contact-info-column">
						<div class="row">
							@if(isset($statistic['13']))
							<div class="contact-info-column-block col-md-4 col-xs-12">
								<div class="block">
									<div class="img"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
									<div class="subtitle">{!!$statistic['13']['title']!!}</div>
									<div class="info">
										{!!$statistic['13']['text']!!}
									</div>
								</div>
							</div>
							@endif
							@if(isset($statistic['14']))
							<div class="contact-info-column-block col-md-4 col-xs-12">
								<div class="block">
									<div class="img"><i class="fa fa-address-book-o" aria-hidden="true"></i></div>
									<div class="subtitle">{!!$statistic['14']['title']!!}</div>
									<div class="info">
										{!!$statistic['14']['text']!!}
									</div>
								</div>
							</div>
							@endif
							@if(isset($statistic['15']))
							<div class="contact-info-column-block col-md-4 col-xs-12">
								<div class="block">
									<div class="img"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
									<div class="subtitle">{!!$statistic['15']['title']!!}</div>
									<div class="info">
									{!!$statistic['15']['text']!!}
									</div>
								</div>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
			<script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBEJKMpH6lYi7wUxzwVyslbr4y85Aq7WVw&libraries=geometry&callback=initMap">
			</script>
			<div class="maps-wrapper">
				<div class="overlay"></div>
				<div id="map"></div>
				
				
			</div>
			<div class="contact-service-wrapper">
				<div class="container">
					@if(isset($statistic['16']))
					<div class="title">	{!!$statistic['16']['title']!!}</div>
					<div class="desc">
						{!!$statistic['16']['text']!!}
					</div>
					@endif
					<div class="contact-form-wrapp">
						<div id="info-status"> @include('common.message')</div>
						<br>
						<form action="{{ url('contact') }}" method="post" class="contact-form">
						{{ csrf_field() }}
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
								      <input type="text" name="name" class="form-control name" id="name" placeholder="Naam" required>
								    </div>
								    <div class="col-sm-6">
								      <input type="email" name="email" class="form-control email" id="email" placeholder="E-mail" required>
								    </div>
								</div>
						  	</div>
						  	<div class="form-group">
						  		<input type="text" name="subject" class="form-control subject" id="subject" placeholder="Onderwerp" required>
						  	</div>
						  	<div class="form-group">
						  		<textarea  id="message" class="message" name="message"cols="64" rows="10" placeholder="Bericht" required></textarea>
						  	</div>
						  	
						  	<div class="form-group">
						  		<div class="col-sm-6">
								    <div id="gocaptcha"></div>
								</div>
								<div class="col-sm-6">
								      <input type="submit" class="send" value="send">
								</div>
						  	</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<script>
		 function initMap() {
        // Create a map object and specify the DOM element for display
		var lat =52.6784572;
		var lang=4.8576828;
		var styleMap = [ { "elementType": "geometry", "stylers": [ { "color": "#f5f5f5" } ] }, { "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "elementType": "labels.text.fill", "stylers": [ { "color": "#616161" } ] }, { "elementType": "labels.text.stroke", "stylers": [ { "color": "#f5f5f5" } ] }, { "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [ { "color": "#bdbdbd" } ] }, { "featureType": "poi", "elementType": "geometry", "stylers": [ { "color": "#eeeeee" } ] }, { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [ { "color": "#e5e5e5" } ] }, { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] }, { "featureType": "road", "elementType": "geometry", "stylers": [ { "color": "#ffffff" } ] }, { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [ { "color": "#dadada" } ] }, { "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [ { "color": "#616161" } ] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] }, { "featureType": "transit.line", "elementType": "geometry", "stylers": [ { "color": "#e5e5e5" } ] }, { "featureType": "transit.station", "elementType": "geometry", "stylers": [ { "color": "#eeeeee" } ] }, { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#c9c9c9" } ] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] } ];
		
		@if($settings['gm_lat']['value'] !='' && $settings['gm_long']['value']!='')
			 lat ={{$settings['gm_lat']['value']}};
		     lang={{$settings['gm_long']['value']}};
		@endif
		
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: lat, lng: lang},
          zoom: 15,
		  styles:styleMap
        });
		new google.maps.Marker({
                                                    position: new google.maps.LatLng(lat,  lang),
                                                    map: map,
													icon:"{{ asset('app/img/map(2).png') }}"
												


                                });
      }
	</script>
@endsection