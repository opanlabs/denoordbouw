@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
   <div id="page">
			@include('common.header')
			<?php $images = $page->images()->get()->toArray(); ?>
			<div class="page-bg-wrapper" @if($images!='') style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}) no-repeat scroll center center / cover !important" @endif>
				<div class="overlay"></div>
				<div class="content-home">
					<div class="container">
						<div class="tagline">
							<h3>{!!$page->intro!!}</h3>
							<h2>{!!$page->text!!}</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="overview-wrapper">
				<div class="container">
					@if(isset($statistic['11']))
					<div class="title">{{$statistic['11']['title']}}</div>
					<div class="desc">{!!$statistic['11']['text']!!}</div>
					@endif
					<div class="overview-column">
						<div class="row">
							<?php $images = $subPages['16']->images()->get()->toArray(); ?>
							<div class="overview-left col-md-6 col-xs-12">
								<div class="overview-block">
									<?php $i=0; ?>
									@foreach ($images as $key=>$row)
										<?php $i++; ?>
									<div class="img{{$i}}">
										@if($i==1)
											<img src="{{ asset('/uploads/images/'. $row['path'] .'/medium.' . $row['extension']) }}" style="width:470px;" alt="" class="img-responsive">
										@else
											<img src="{{ asset('/uploads/images/'. $row['path'] .'/medium.' . $row['extension']) }}" style="width:270px;"  alt="" class="img-responsive">
										@endif								
									</div>
									@endforeach
									
								</div>
							</div>
							<div class="overview-right col-md-6 col-xs-12">
								@if(isset($subPages['16']))
									<div class="overview-block">
										<div class="title">{{$subPages['16']['title']}}</div>
										<div class="desc">
											{!!$subPages['16']['text']!!}
										</div>
									</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
			@if(isset($subPages['15']))
			<div class="history-wrapper">
				<div class="container">
					<div class="history-left col-md-6 col-xs-12">
					
						<div class="history-block">
							<div class="title">{{$subPages['15']['title']}}</div>
							<div class="desc">
								{!!$subPages['15']['text']!!}
							</div>
						</div>
					</div>
					<div class="history-right col-md-6 col-xs-12">
						<div class="history-block">
							<div class="img"><img src="{{ asset('/app/img/img-history.jpg') }}" alt="" class="img-responsive"></div>
							<!-- <div class="cs-block">
								<div class="row">
									<div class="cs pull-left"><img src="{{ asset('/app/img/ico-cs.png') }}" alt="" class="img-responsive"></div>
									<div class="cs pull-left">
										<div class="time">24hr Hotline</div>
										<div class="phone">06-1135-8177</div>
									</div>
								</div>
							</div> -->
						</div>
					</div>
				</div>
			</div>
			@endif
			@if(isset($statistic['10']))
				<div class="renovation-wrapper">
					<div class="container">
						<div class="title">{{$statistic['10']['title']}} </div>
						<div class="desc">{!!$statistic['10']['text']!!}  </div>
					</div>
				</div>
			@endif
			<div class="service-static-wrapper">
				<div class="container">
					<div class="row">
						@if(isset($statistic['1']))
						<?php $images = $statistic['1']->images()->get()->toArray(); ?>
							<div class="service-static-block col-md-4">
								<div class="service-static">
									<a href="{{$statistic['1']['slug']}}">
										<div class="img"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
										<div class="title">{!!$statistic['1']['title']!!}</div>
										<div class="desc">
											{!!$statistic['1']['text']!!}
										</div>
									</a>
								</div>
							</div>
						@endif
						@if(isset($statistic['2']))
							<?php $images = $statistic['2']->images()->get()->toArray(); ?>
						<div class="service-static-block col-md-4">
							<div class="service-static">
								<a href="{{$statistic['2']['slug']}}">
									<div class="img"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
									<div class="title">{!!$statistic['2']['title']!!}</div>
									<div class="desc">
									{!!$statistic['2']['text']!!}
									</div>
								</a>							
							</div>
						</div>
						@endif
						@if(isset($statistic['3']))
								<?php $images = $statistic['3']->images()->get()->toArray(); ?>
						<div class="service-static-block col-md-4">
							<div class="service-static">
								<a href="{{$statistic['3']['slug']}}">
									<div class="img"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
									<div class="title">{!!$statistic['3']['title']!!}</div>
									<div class="desc">
									{!!$statistic['3']['text']!!}
									</div>
								</a>
							</div>
						</div>
						@endif
					</div>
					<div class="row">
						<div class="seeall"><a href="{{url('diensten')}}">Bekijk alle</a></div>
					</div>
				</div>				
			</div>
		</div>
@endsection