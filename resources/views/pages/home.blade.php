@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
   <div id="home">
			@include('common.header')
			<div class="home-bg-wrapper" style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $banner[0]['images']['path'] .'/banner.' . $banner[0]['images']['extension']) }}) no-repeat scroll center center / cover !important">
				<div class="overlay"></div>
				<div class="content-home">
					<div class="container">
						<div class="tagline" style="">
							<h3></h3>
							<h2>{{$banner[0]['title']}}</h2>
							<div class="desc">{!!$banner[0]['description']!!}</div>
							@if(isset($statistic['21']))
							<div class="readmore"><a href="{!!$statistic['21']['slug']!!}">{!!$statistic['21']['text']!!}</a></div>
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="service-static-wrapper">
				<div class="container">
					<div class="row">
						@if(isset($statistic['1']))
						<?php $images = $statistic['1']->images()->get()->toArray(); ?>
							<div class="service-static-block col-md-4">
								<div class="service-static">
									<a href="{{$statistic['1']['slug']}}">
										<div class="img"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
										<div class="title">{!!$statistic['1']['title']!!}</div>
										<div class="desc">
											{!!$statistic['1']['text']!!}
										</div>
									</a>
								</div>
							</div>
						@endif
						@if(isset($statistic['2']))
							<?php $images = $statistic['2']->images()->get()->toArray(); ?>
						<div class="service-static-block col-md-4">
							<div class="service-static">
								<a href="{{$statistic['2']['slug']}}">
									<div class="img"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
									<div class="title">{!!$statistic['2']['title']!!}</div>
									<div class="desc">
									{!!$statistic['2']['text']!!}
									</div>
								</a>							
							</div>
						</div>
						@endif
						@if(isset($statistic['3']))
								<?php $images = $statistic['3']->images()->get()->toArray(); ?>
						<div class="service-static-block col-md-4">
							<div class="service-static">
								<a href="{{$statistic['3']['slug']}}">
									<div class="img"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
									<div class="title">{!!$statistic['3']['title']!!}</div>
									<div class="desc">
									{!!$statistic['3']['text']!!}
									</div>
								</a>
							</div>
						</div>
						@endif
					</div>
				</div>				
			</div>
			@if(isset($statistic['4']))
					<?php $images = $statistic['4']->images()->get()->toArray(); ?>
			<div class="call-service-wrapper">
				<div class="container">
					<div class="title">{!!$statistic['4']['title']!!}, </div>
					<div class="desc">{!!$statistic['4']['text']!!} </div>
					<div class="img"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
				</div>
			</div>
			@endif
			@if(isset($statistic['5']))
			<div class="about-block-wrapper">
				<div class="container">
					<div class="row">
						<div class="about-block-left col-md-7">
							<div class="about-block-column">
								<div class="about-block-column-img">
								<?php $images = $statistic['5']->images()->get()->toArray(); ?>
								@if(count($images)>0)
								<div class="col-left">
										<div class="img"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
										<div class="img"><img src="{{ asset('/uploads/images/'. $images[1]['path'] .'/original.' . $images[1]['extension']) }}" alt="" class="img-responsive"></div>			
									</div>
									<div class="col-right">
										<div class="img"><img src="{{ asset('/uploads/images/'. $images[2]['path'] .'/original.' . $images[2]['extension']) }}" alt="" class="img-responsive"></div>
										<div class="img"><img src="{{ asset('/uploads/images/'. $images[3]['path'] .'/original.' . $images[3]['extension']) }}" alt="" class="img-responsive"></div>
									</div>
									<div class="clear"></div>
								</div>
								@endif
								<div class="title-hori">about us</div>
							</div>
						</div>
						<div class="about-block-right col-md-5">
							<div class="about-block-column">
								<div class="title">{!!$statistic['5']['title']!!}</div>
								<div class="desc">
									{!!$statistic['5']['text']!!}
								</div>
								<div class="readmore"><a href="{{$statistic['5']['slug']}}">Lees meer</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			<!--
			<div class="contact-service-wrapper">
				<div class="container">
					<div class="title">{!!(isset($statistic['9']))?$statistic['9']['title']:''!!}</div>
					<div class="desc">
						{!!(isset($statistic['9']))?$statistic['9']['text']:''!!}
					</div>
					<div class="contact-form-wrapp">
						<form action="{{ url('contact') }}" method="post" class="contact-form">
							{{ csrf_field() }}
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
								      <input type="text" name="name" class="form-control name" id="name" placeholder="Naam" required>
								    </div>
								    <div class="col-sm-6">
								      <input type="email" name="email" class="form-control email" id="email" placeholder="E-mail" required>
								    </div>
								</div>
						  	</div>
						  	<div class="form-group">
						  		<input type="text" name="subject" class="form-control subject" id="subject" placeholder="Onderwerp" required>
						  	</div>
						  	<div class="form-group">
						  		<textarea  id="message" class="message" name="message"cols="64" rows="10" placeholder="Bericht" required></textarea>
						  	</div>
						  	<div class="form-group">
						  		<div id="gocaptcha"></div>
						  	</div>
						  	<div class="form-group">
						  		<input type="submit" class="send" value="send">
						  	</div>
						</form>
					</div>
				</div>
			</div>
			-->
			<div class="testimoni-home-wrapper">
				<div class="overlay"></div>
				<div class="testimoni-home-block">
					<div class="container">
						<div class="title">{!!(isset($statistic['20']))?$statistic['20']['title']:''!!}</div>
						<div class="quote"><img src="{{ asset('/app/img/quote.png') }}" alt="" class="img-responsive"></div>
						<div class="testimoni-slide-column slider">
							@foreach($referensi as $key=>$row)
							<?php $images = $row->images()->get()->toArray(); ?>
							<div class="testimoni-slide">
								<div class="desc">
								{!!$row['text']!!}
								</div>
								<div class="user">
									<div class="avatar"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
									<div class="user-detail">
										<div class="name">{!!$row['name']!!}</div>
										<div class="position">{!!$row['position']!!}</div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection