@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
    <div id="page">
			@include('common.header')
			<?php $images = $page->images()->get()->toArray(); ?>
			
			<div class="page-bg-wrapper" @if(isset($images[0])) style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}) no-repeat scroll center center / cover !important" @endif>
				<div class="overlay"></div>
				<div class="content-home">
					<div class="container">
						<div class="tagline">
							<h3>{{$page->meta_title}}</h3>
							<h2>{{$page->meta_description}}</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="inner-detail-wrapper">
				<div class="container">
					<div class="row">
						<div class="column-left col-md-4">
							<div class="widget">
								<div class="menus satu">
									<ul>
										@foreach($subPagesNavigation as $key=>$row)
										<li @if($page->id==$row['id']) class="active" @endif @if($row['icon']!='') style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $row['icon'] .'/icon.' .$row['icon_extension']) }})  no-repeat scroll 5% center !important" @endif><a href="{{$row['slug']}}">{{$row['title']}}</a></li>
										@endforeach
									</ul>
								</div>
							</div>
							<div class="widget">
								<div class="title">CONTACT</div>
								<div class="menus dua">
									<ul>
										<li><div class="ico"><i class="fa fa-map-marker" aria-hidden="true"></i></div>{{ $settings['address']->value }}</li>
										<li>
											<a href="tel:{{ $settings['phone']->value }}">
											<div class="ico"><i class="fa fa-phone" aria-hidden="true"></i></div>
											{{ $settings['phone']->value }}</a>
										</li>
										<li>
											<a href="tel:{{ $settings['mobile']->value }}">
											<div class="ico"><i class="fa fa-mobile" aria-hidden="true"></i></div>
											{{ $settings['mobile']->value }}</a>
										</li>
										<li>
											<a href="mailto:{{ $settings['email']->value }}">
											<div class="ico"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
											{{ $settings['email']->value }}</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="column-right col-md-8">
							<div class="content">
							
							
								
							
								
									@if(count($images)>0)
										<div class="slider slider-for">
											
												@foreach($images as $key=>$row)
												<div class="large"><img src="{{ asset('/uploads/images/'. $row['path'] .'/big.' . $row['extension']) }}" alt="" class="img-responsive"></div>
												@endforeach
											
										</div>
										<div class="slider slider-nav">
											@foreach($images as $key=>$row)
											
											<div class="thumb"><img src="{{ asset('/uploads/images/'. $row['path'] .'/small.' . $row['extension']) }}" alt="" class="img-responsive"></div>
											@endforeach
											

										</div>
									@endif
								<div class="content-txt">
									
											<div class="subtitle">{{$page->title}}</div>
											<p>{!!$page->text!!}</p>

											<p>&nbsp;</p>
								
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-white">
				<div class="back"><a href="diensten">Ga terug naar alle diensten</a></div>
			</div>
		</div>
@endsection