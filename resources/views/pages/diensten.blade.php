@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
   <div id="page">
			@include('common.header')
			<?php $images = $page->images()->get()->toArray(); ?>
			<div class="page-bg-wrapper" @if($images!='') style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}) no-repeat scroll center center / cover !important" @endif>
				<div class="overlay"></div>
				<div class="content-home">
					<div class="container">
						<div class="tagline">
							<h3>{!!$page->intro!!}</h3>
							<h2>{!!$page->text!!}</h2>
						</div>
					</div>
				</div>
			</div>
			@if(isset($statistic['17']))
			<div class="renovation-wrapper">
				<div class="container">
					<div class="title">{{$statistic['17']['title']}}</div>
					<div class="desc">{!!$statistic['17']['text']!!}</div>
				</div>
			</div>
			@endif
			<div class="service-static-wrapper">
				<div class="container">
					<div class="row">
						<?php $i =0; ?>
						@foreach($subPages as $key=>$row)
							
							@if($i < 3)
								<div class="service-static-block col-md-4">
									<div class="service-static">
										<a href="{{$row['slug']}}">
									
											@if($row['icon']!='')
												<div class="img"><img src="{{ asset('/uploads/images/'. $row['icon'] .'/original.' .$row['icon_extension']) }}" alt="" class="img-responsive"></div>
											@endif
											<div class="title">{{$row['title']}}</div>
											<div class="desc">
												{!! str_limit($row['intro'],70)!!}
											</div>
										</a>
									</div>	
								</div>
							@endif
							<?php $i++; ?>
						@endforeach
						
					</div>
					<div class="second-column">
						<div class="row">
							@if(isset($statistic['18']))
							<div class="service-static-block col-md-4">
								<div class="service-static">
									<div class="subtitle">{{$statistic['18']['title']}}</div>
									<div class="subtitle2">{!!$statistic['18']['text']!!}</div>
								</div>	
							</div>
							@endif
							<?php $i =0; ?>
							@foreach($subPages as $key=>$row)
								@if($i >= 3)
								<div class="service-static-block col-md-4">
									<div class="service-static">
										<a href="{{$row['slug']}}">
											@if($row['icon']!='')
												<div class="img"><img src="{{ asset('/uploads/images/'. $row['icon'] .'/original.' . $row['icon_extension']) }}" alt="" class="img-responsive"></div>
											@endif
											<div class="title">{{$row['title']}}</div>
											<div class="desc">
												{!! str_limit($row['intro'],70)!!}
											</div>
										</a>
									</div>	
								</div>
								@endif
								<?php $i++; ?>
							@endforeach
							
						</div>
					</div>
				</div>				
			</div>
			@if(isset($statistic['19']))
			<div class="bottom-bg-wrapper">
				<div class="container">
					<div class="content">
						<div class="tagline">
							<h3>{{$statistic['19']['title']}}</h3>
							<div class="desc">{!!$statistic['19']['text']!!}</div>
							<div class="readmore"><a href="{{$statistic['19']['slug']}}">Neem contact met ons op</a></div>
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
@endsection