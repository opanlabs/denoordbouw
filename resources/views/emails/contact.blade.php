<html>
<head>
    <title>Onderstaande gegevens zijn verzonden via {{ $config['company']['value'] }}</title>
</head>
<body>
<table width="100%">
    <tr>
        <td>Naam: </td><td>{!! $name !!}</td>
    </tr>
    <tr>
        <td>E-mail: </td><td>{!! $email !!}</td>
    </tr>
    @if (isset($subject))
        <tr>
            <td>Onderwerp: </td><td>{!! $subject !!}</td>
        </tr>
    @endif
    <tr>
        <td>Bericht: </td><td>{!! $msg !!}</td>
    </tr>
</table>
</body>
</html>