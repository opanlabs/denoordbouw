<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Repositories\Contracts\BannerRepository;
use App\Repositories\Contracts\PageRepository;
use App\Repositories\Contracts\BlockRepository;
use App\Repositories\Contracts\ReferentiesRepository;
use App\Repositories\Criteria\IsActive;
use App\Repositories\Criteria\IsRoot;
use Illuminate\Http\Request;
use Validator;
// use Illuminate\View\Engines\CompilerEngine;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use DB;

class PagesController extends Controller
{
    /**
     * @var static vars
     */
    protected $settings, $navigation, $banner;

    /**
     * @var repository vars
     */
    protected $pageRepository, $bannerRepository;

    public function __construct(PageRepository $pageRepository,
								BlockRepository $blockRepository,
                                BannerRepository $bannerRepository,
								ReferentiesRepository $referentiesRepository)
    {
        $this->settings = Setting::select('field', 'value')->get()->keyBy('field');
        $this->pageRepository = $pageRepository;
        $this->bannerRepository = $bannerRepository;
		 $this->blockRepository =  $blockRepository;
		 $this->referentiesRepository =  $referentiesRepository;
        // $this->navigation = $this->loadNavigationItems();
    }

    /**
     * Load the navigation items
     * @return mixed
     */
    public function loadNavigationItems()
    {
        $items = $this->pageRepository->getByCriteria(new IsActive(), new IsRoot());
        return $items;
    }

    /**
     * Display the home page
     * @return View
     */
    public function home()
    {
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		foreach($data['navigation'] as $key=>$row)
		{
			if($row['id']==4){
				$data['navigation'][$key]['subpages']	=  $this->pageRepository->findByField('parent_id',$row['id'])->where('active',1);
			}
			else
			{
				$data['navigation'][$key]['subpages']	= array();
			}
			
		}
		$data['banner']		=$this->bannerRepository->findByField('id',1)->where('active',1);
		if(isset($data['banner'][0]))
		{
			$data['banner'][0]['images']	=$data['banner'][0]->images()->first();
		}
		
		$data['referensi']		=$this->referentiesRepository->all()->where('active',1);
		
		$data['page']		=$this->pageRepository->findBySlug('home');
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[1,2,3,4,5,7,9,20,21])->where('active',1)->keyBy('id');
		// echo"<pre>";print_r($data['referensi']->toArray());die;
        return view('pages.home', $data);
    }
	
	/**
     * Display the home page
     * @return View
     */
    public function overOns()
    {
		// echo'sssd1111';die;
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		foreach($data['navigation'] as $key=>$row)
		{
			if($row['id']==4){
				$data['navigation'][$key]['subpages']	=  $this->pageRepository->findByField('parent_id',$row['id'])->where('active',1);
			}
			else
			{
				$data['navigation'][$key]['subpages']	= array();
			}
		}
		$data['page']		=$this->pageRepository->findBySlug('overons');
		$data['subPages']	= $this->pageRepository->findByField('parent_id',$data['page']['id'])->keyBy('id')->where('active',1);
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[1,2,3,5,7,10,11])->where('active',1)->keyBy('id');
		// echo"<pre>";print_r($data->toArray());die;
        return view('pages.overons', $data);
    }
	
	
	
    /**
     * Display a default page
     * @param $slug
     * @return View
     */
    public function defaultPage($slug)
    {
		// echo $slug;die;
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		foreach($data['navigation'] as $key=>$row)
		{
			if($row['id']==4){
				$data['navigation'][$key]['subpages']	=  $this->pageRepository->findByField('parent_id',$row['id'])->where('active',1);
			}
			else
			{
				$data['navigation'][$key]['subpages']	= array();
			}
		}
		
		$data['page']		=$this->pageRepository->findBySlug($slug);
		$data['subPagesNavigation']	= $this->pageRepository->findByField('parent_id',$data['page']['parent_id'])->where('active',1);
		$data['subPages']	= $this->pageRepository->findByField('parent_id',$data['page']['id'])->where('active',1);
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[1,2,3,5,7,10,11])->where('active',1)->keyBy('id');
		// echo"<pre>";print_r($data->toArray());die;
        return view('pages.default', $data);
		
    }

    /**
     * Display the contact page
     * @return View
     */
    public function contact()
    {
		// echo'ssss';die;
		$data['settings']	=$this->settings;
		
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		foreach($data['navigation'] as $key=>$row)
		{
			if($row['id']==4){
				$data['navigation'][$key]['subpages']	=  $this->pageRepository->findByField('parent_id',$row['id'])->where('active',1);
			}
			else
			{
				$data['navigation'][$key]['subpages']	= array();
			}
		}
		$data['page']		=$this->pageRepository->findBySlug('contact');
		$data['subPages']	= $this->pageRepository->findByField('parent_id',$data['page']['id'])->keyBy('id')->where('active',1);
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[5,7,10,12,13,14,15,16])->where('active',1)->keyBy('id');
		// echo"<pre>";print_r($data->toArray());die;
        return view('pages.contact', $data);
       
    }
	/**
     * Display the contact page
     * @return View
     */
    public function diensten()
    {
		// echo'ssss';die;
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		foreach($data['navigation'] as $key=>$row)
		{
			if($row['id']==4){
				$data['navigation'][$key]['subpages']	=  $this->pageRepository->findByField('parent_id',$row['id'])->where('active',1);
			}
			else
			{
				$data['navigation'][$key]['subpages']	= array();
			}
		}
		$data['page']		=$this->pageRepository->findBySlug('diensten');
		$data['subPages']	= $this->pageRepository->findByField('parent_id',$data['page']['id'])->keyBy('id')->where('active',1);
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[5,7,10,17,18,19])->where('active',1)->keyBy('id');
		// echo"<pre>";print_r($data->toArray());die;
        return view('pages.diensten', $data);
       
    }

    /**
     * Send the contact form
     * @param Requests\SendContactRequest $request
     * @return Response
     */
    public function sendContact(Request $request)
    {
		$validator = Validator::make(Input::all(), 
			array(
				'email'             => 'required|max:50|email',
				'name'         		=> 'required',
				'subject'          => 'required',
				'message'   			 => 'required',
				'g-recaptcha-response' => 'required|recaptcha'
				
			),
			array (
				'name.required' => 'Vul je naam in.',
				'email.required' => 'Vul je e-mailadres in.',
				'email.email' => 'Vul een geldig e-mailadres in.',
				'message.required' => 'Vul een vraag of opmerking in.',
				'g-recaptcha-response.required' => 'De reCaptcha is verplicht.',
				'g-recaptcha-response.recaptcha' => 'Verifieer dat u geen robot bent.'
			)
		);
		if($validator->fails()){
			// echo "<pre>";print_r($validator);die;
			return redirect()->back()->withErrors($validator)->withInput(Input::all());;
		} else {
			$config = $this->settings;
			
			$data = [
				'name' => Input::get('name'),
				'email' => Input::get('email'),
				'subject' => Input::get('subject'),
				'msg' => Input::get('message'),
				'config' => $config
			];
			

			Mail::send('emails.contact', $data, function($message) use ($config) {
				$message->from($config['email']['value'], $config['company']['value']);
				$message->to($config['email']['value'], $config['company']['value']);
				$message->subject('Contactformulier');
			});

			return redirect()->back()->with('success', 'Uw bericht is verstuurd.');
		}
    }
}
