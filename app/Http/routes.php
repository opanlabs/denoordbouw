<?php

Route::any('/', 'PagesController@home');
Route::any('/home', 'PagesController@home');
Route::any('overons', 'PagesController@overOns');
Route::any('/contact', 'PagesController@contact');
Route::post('/contact', 'PagesController@sendContact');
Route::any('/diensten', 'PagesController@diensten');
Route::any('{slug}', 'PagesController@defaultPage'); 

