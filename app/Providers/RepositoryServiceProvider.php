<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('\\App\\Repositories\\Contracts\\ModuleRepository', 'App\\Repositories\\Eloquent\\ModuleRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\UserRepository', 'App\\Repositories\\Eloquent\\UserRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\RoleRepository', 'App\\Repositories\\Eloquent\\RoleRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\PermissionRepository', 'App\\Repositories\\Eloquent\\PermissionRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\LanguageRepository', 'App\\Repositories\\Eloquent\\LanguageRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\SettingRepository', 'App\\Repositories\\Eloquent\\SettingRepositoryEloquent');

        /* Module Repositories */
        $this->app->bind('\\App\\Repositories\\Contracts\\PageRepository', 'App\\Repositories\\Eloquent\\PageRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\BlockRepository', 'App\\Repositories\\Eloquent\\BlockRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\CategoryRepository', 'App\\Repositories\\Eloquent\\CategoryRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\AttributeRepository', 'App\\Repositories\\Eloquent\\AttributeRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\AttributeSetRepository', 'App\\Repositories\\Eloquent\\AttributeSetRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\ProductRepository', 'App\\Repositories\\Eloquent\\ProductRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\NewsRepository', 'App\\Repositories\\Eloquent\\NewsRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\BannerRepository', 'App\\Repositories\\Eloquent\\BannerRepositoryEloquent');
        $this->app->bind('\\App\\Repositories\\Contracts\\ImageRepository', 'App\\Repositories\\Eloquent\\ImageRepositoryEloquent');
		$this->app->bind('\\App\\Repositories\\Contracts\\ReferentiesRepository', 'App\\Repositories\\Eloquent\\ReferentiesRepositoryEloquent');
    }
}
