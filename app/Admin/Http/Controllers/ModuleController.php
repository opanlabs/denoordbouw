<?php

namespace Admin\Http\Controllers;

use App\Repositories\Contracts\ModuleRepository;
use Illuminate\Support\Facades\Input;

class ModuleController extends Controller
{
    /**
     * @var ModuleRepository
     */
    protected $repository;

    /**
     * ModuleController constructor.
     *
     * @param ModuleRepository $repository
     */
    public function __construct(ModuleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin::pages.module.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::pages.module.edit', [
            'module' => $this->repository->find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $this->repository->update(['config' => json_decode(Input::get('config'))], $id);
        return redirect()->route('cms.module.index');
    }

    /**
     * Return list with module data.
     *
     * @return mixed
     */
    public function data()
    {
        return $this->repository->getForDatatable(['active', 'edit'], false);
    }

    /**
     * Update the sequence
     */
    public function reorder()
    {
        $this->repository->reorder(Input::get('order'));
    }

    /**
     * Change the activity status
     *
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status($id, $status)
    {
        $this->repository->activate($status, $id);
        return redirect()->route('cms.module.index');
    }
}