<?php

namespace Admin\Http\Controllers;

use App\Repositories\Contracts\RoleRepository;
use App\Repositories\Contracts\UserRepository;
use App\Repositories\Criteria\IsNotSuperadmin;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin::pages.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->roleRepository->pushCriteria(new IsNotSuperadmin());
        return view('admin::pages.user.create', [
            'roles' => $this->roleRepository->getForSelect('display_name', 'id', 'display_name', 'ASC')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            Input::merge(['sequence' => $this->userRepository->all()->max('sequence') + 1]);
            $user = $this->userRepository->create(Input::all());
            $user->attachRoles(Input::get('roles'));
        } catch (ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.user.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        $this->roleRepository->pushCriteria(new IsNotSuperadmin());
        $roles = $this->roleRepository->getForSelect('display_name', 'id', 'display_name', 'ASC');
        $selected = $user->roles->lists('id')->toArray();

        return view('admin::pages.user.edit', [
            'user' => $user,
            'roles' => $roles,
            'selected' => $selected
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        try {
            $user = $this->userRepository->update(Input::all(), $id);
            $user->roles()->sync(Input::get('roles'));
        } catch(ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->userRepository->delete($id);

        return Response::json([
            'status'    => 'success',
            'tree'      => null
        ], 200);
    }

    /**
     * Return list with module data.
     *
     * @return mixed
     */
    public function data()
    {
        return $this->userRepository->getForDatatable(['active', 'edit', 'delete']);
    }

    /**
     * Update the sequence
     */
    public function reorder()
    {
        $this->userRepository->reorder(Input::get('order'));
    }

    /**
     * Change the activity status
     *
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status($id, $status)
    {
        $this->userRepository->activate($status, $id);
        return redirect()->route('cms.user.index');
    }
}