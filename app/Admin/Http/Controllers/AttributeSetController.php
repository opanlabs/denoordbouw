<?php

namespace Admin\Http\Controllers;

use App\Repositories\Contracts\AttributeRepository;
use App\Repositories\Contracts\AttributeSetRepository;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Exceptions\ValidatorException;

class AttributeSetController extends Controller
{
    /**
     * @var AttributeSetRepository
     */
    protected $attributeSetRepository;

    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    public function __construct(AttributeSetRepository $attributeSetRepository, AttributeRepository $attributeRepository)
    {
        $this->attributeSetRepository   = $attributeSetRepository;
        $this->attributeRepository      = $attributeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin::pages.components.attributeset.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin::pages.components.attributeset.create', [
            'attributes' => $this->attributeRepository->all()->lists('name', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            Input::merge(['sequence' => $this->attributeSetRepository->all()->max('sequence') + 1]);
            $attributeSet = $this->attributeSetRepository->create(Input::all());
            $attributeSet->attributes()->attach(Input::get('attributes'));
        } catch (ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.attributeset.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::pages.components.attributeset.edit', [
            'attributeset' => $set = $this->attributeSetRepository->find($id),
            'attributes' => $this->attributeRepository->all()->lists('name', 'id'),
            'selected' => $set->attributes->lists('id')->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        try {
            $attributeSet = $this->attributeSetRepository->update(Input::all(), $id);
            $attributeSet->attributes()->sync(Input::get('attributes'));
        } catch(ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.attributeset.index');
    }

    /**
     * Return list with resource data.
     *
     * @return mixed
     */
    public function data()
    {
        return $this->attributeSetRepository->getForDatatable(['edit', 'delete']);
    }
}