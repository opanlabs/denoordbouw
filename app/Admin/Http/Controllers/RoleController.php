<?php

namespace Admin\Http\Controllers;

use App\Repositories\Contracts\PermissionRepository;
use App\Repositories\Contracts\RoleRepository;
use App\Repositories\Criteria\IsNotSuperadmin;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class RoleController extends Controller
{
    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * @var PermissionRepository
     */
    protected $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin::pages.role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin::pages.role.create', [
            'permissions' => $this->permissionRepository->getForSelect('display_name', 'id', 'display_name', 'ASC')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            Input::merge(['sequence' => $this->roleRepository->all()->max('sequence') + 1]);
            $role = $this->roleRepository->create(Input::all());
            $role->permissions()->sync(Input::get('permissions'));
        } catch (ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.role.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::pages.role.edit', [
            'role' => $role = $this->roleRepository->find($id),
            'permissions' => $this->permissionRepository->getForSelect('display_name', 'id', 'display_name', 'ASC'),
            'selected' => $role->permissions->lists('id')->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        try {
            $role = $this->roleRepository->update(Input::all(), $id);
            $role->permissions()->sync(Input::get('permissions'));
        } catch(ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->roleRepository->delete($id);

        return Response::json([
            'status'    => 'success',
            'tree'      => null
        ], 200);
    }

    /**
     * Return list with module data.
     *
     * @return mixed
     */
    public function data()
    {
        $this->roleRepository->pushCriteria(new IsNotSuperadmin());
        return $this->roleRepository->getForDatatable(['edit', 'delete']);
    }

    /**
     * Update the sequence
     */
    public function reorder()
    {
        $this->roleRepository->reorder(Input::get('order'));
    }
}