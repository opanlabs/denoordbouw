<?php

namespace Admin\Http\Controllers;

use App\Repositories\Contracts\AttributeRepository;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Exceptions\ValidatorException;

class AttributeController extends Controller
{
    /**
     * @var AttributeRepository
     */
    protected $repository;

    public function __construct(AttributeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin::pages.components.attribute.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin::pages.components.attribute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            Input::merge(['sequence' => $this->repository->all()->max('sequence') + 1]);
            $this->repository->create(Input::all());
        } catch (ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.attribute.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::pages.components.attribute.edit', [
            'attribute' => $this->repository->find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        try {
            $this->repository->update(Input::all(), $id);
        } catch(ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.attribute.index');
    }

    /**
     * Return list with resource data.
     *
     * @return mixed
     */
    public function data()
    {
        return $this->repository->getForDatatable(['edit', 'delete']);
    }
}