<?php

namespace Admin\Http\Controllers;

use App\Models\Setting;
use App\Repositories\Contracts\LanguageRepository;
use App\Repositories\Contracts\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    /**
     * @var LanguageRepository
     */
    protected $languageRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Set the current locale
     *
     * @param $code
     * @param LanguageRepository $repository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setLocale($code, LanguageRepository $repository)
    {
        $this->languageRepository = $repository;

        $lang = $this->languageRepository->findByField('code', $code)->first();

        app()->setLocale($code);

        if(!Session::has('locale')) {
            Session::put('locale', $lang);
        } else {
            Session::set('locale', $lang);
        }

        return redirect()->back();
    }

    /**
     * Display settings page.
     *
     * @return Response
     */
    public function defaultSettings()
    {
        $settings = Setting::all()->keyBy('field');

        return view('admin::pages.setting.settings', compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $settings = Input::except(['_method', '_token']);

        foreach ($settings as $field => $value) {
            $setting = Setting::whereField($field)->first();
            $setting->value = $value;
            $setting->update();
        }

        return redirect()->back();
    }

    /**
     * Edit users profile
     */
    public function profile()
    {
        return view('admin::pages.setting.profile', [
            'user' => $this->userRepository->find(Auth::user()->id)
        ]);
    }
}