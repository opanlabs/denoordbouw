<?php

namespace Admin\Http\Controllers;

use Admin\Facades\Module;
use App\Repositories\Contracts\ImageRepository;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Intervention\Image\Exception\NotWritableException;
use Intervention\Image\Facades\Image;
use Prettus\Validator\Exceptions\ValidatorException;
use Activity;

class ImageController extends Controller
{
    /**
     * @var ImageRepository
     */
    protected $repository;

    public function __construct(ImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Upload images
     *
     * @return mixed
     */
    public function upload()
    {
        // Check if there is an image
        if ($image = Image::make(Input::file('file'))) {
            // Upload path
            $uploadPath = public_path() . DIRECTORY_SEPARATOR . 'uploads';
            $path       = $uploadPath . DIRECTORY_SEPARATOR . 'images';

            // Image information
            $extension  = pathinfo(Input::file('file')->getClientOriginalName(), PATHINFO_EXTENSION);
            $hash       = sha1(str_random(10) . Input::file('file')->getClientOriginalName() . time());
            $fileSize   = $image->filesize();
            $original   = Input::file('file')->getClientOriginalName();

            // Upload folder
            $folder     = $path . DIRECTORY_SEPARATOR . $hash . DIRECTORY_SEPARATOR;

            // Create folders if they don't exist
            if (!File::isDirectory($uploadPath)) File::makeDirectory($uploadPath, 0777);
            if (!File::isDirectory($path)) File::makeDirectory($path, 0777);
            File::makeDirectory($folder, 0777);

            // Save the original image
            try {
                $image->save($folder . 'original' . '.' .  $extension);
            } catch (NotWritableException $e) {
                throw new NotWritableException;
            }

            // Load the configuration
            if ($module = Module::get(Input::get('module'))) {
                // Loop through all the configured image sizes
				// echo"<pre>";print_r($module);die;
                foreach ($module['settings']['images']['sizes'] as $name => $config) {
                    // Create the thumbnail/image
                    try {
                        $thumb = Image::make(Input::file('file'));
                        if(isset($config['type']) && $config['type']=='fit'){
                            $thumb->fit($config['width'], $config['height'], function($constraint) {
                                //$constraint->aspectRatio();
                            });
                        }else{
                            $thumb->resize($config['width'], $config['height'], function($constraint) {
                                $constraint->aspectRatio();
                            });    
                        }
                        $thumb->save($folder . $name . '.' . $extension, 100);
                    } catch (NotWritableException $e) {
                        throw new NotWritableException;
                    }
                }
            }

            // Save image to the database
            $img = $this->repository->create([
                'imageable_id'      => Input::get('imageable_id'),
                'imageable_type'    => Input::get('imageable_type'),
                'path'              => $hash,
                'size'              => $fileSize,
                'extension'         => $extension,
                'name'              => '',
                'description'       => '',
                'original_name'     => Input::file('file')->getClientOriginalName(),
                'sequence'          => $this->repository->all()->max('sequence') + 1,
                'active'            => 1
            ]);

            // Save the image polymorphic relation
            $repoName = "\\App\\Repositories\\Contracts\\" . ucfirst(strtolower(Input::get('imageable_type'))) . "Repository";
            $instance = app()->make($repoName);
            $instance->find((int) Input::get('imageable_id'))->images()->save($img);

            Activity::log("Image '{$original}' uploaded with a size of: {$fileSize}");

            return Response::json([
                'status' => 'success',
                'id'     => $img->id
            ], 200);
        }
    }

    /**
     * Delete an image
     */
    public function delete()
    {
        // Find the image
        $image = $this->repository->find(Input::get('id'));

        // Path information
        $path   = public_path() . '/uploads/images/';
        $folder = $path . $image->path;

        // Delete the image folder if exists
        if(!File::deleteDirectory($folder)) Response::json([
            'status'    => 'error',
            'message'   => 'Afbeeldingsmap niet gevonden.'
        ], 500);

        // Delete the image from the database
        $image->delete();

        Activity::log("Image({$image->id}) '{$image->name}' deleted");

        return Response::json([
            'status' => 'success',
            'message' => 'Afbeelding verwijderd'
        ], 200);
    }

    /**
     * Set the image order
     */
    public function reorder()
    {
        $images = [];

        // Check if order is given
        if ($order = Input::get('order')) {
            // Loop through the new order
            for ($i = 0; $i < count($order); $i++) {
                $img = $this->repository->find($order[$i]);
                $img->update(['sequence' => ($i + 1)]);
            }
        }

        Activity::log("Images reordered");

        return Response::json([
            'status' => 'success',
            'message' => 'Order updated'
        ], 200);
    }

    /**
     * Edit the image
     *
     * @param $id
     */
    public function edit($id)
    {
        $image = $this->repository->find($id);

        return View::make('admin::common.image-modal', compact('image'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        try {
            $image = $this->repository->update(Input::all(), $id);
            Activity::log("Image({$image->id}) '{$image->name}' updated");
        } catch(ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->back();
    }
}