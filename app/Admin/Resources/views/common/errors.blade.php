@if (count($errors) > 0)
<!-- Form Error List -->
<div class="alert alert-danger">
    <strong>{{ trans('admin::messages.errors.default') }}</strong>

    <br><br>

    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif