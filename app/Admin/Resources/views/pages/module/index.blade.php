@extends('admin::layouts.admin')

@section('title', 'Modules')

@section('content')
    <div class="padding">
        <div class="box">
            <div class="box-header">
                <h2>Modules</h2>
            </div>
            <div class="table-responsive">
                <table id="modules-table" data-type="module" class="table table-striped b-t b-b" width="100%">
                    <thead>
                    <tr>
                        <th style="width:10%">Volgorde</th>
                        <th style="width:70%">Naam</th>
                        <th style="width:10%"></th>
                        <th style="width:10%"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        var oTable = $('#modules-table').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            rowReorder: {
                dataSrc: 'sequence'
            },
            ajax: {
                url: '{!! url('cms/module/data') !!}',
                type: 'POST',
                data: { _token: '{!! csrf_token() !!}' }
            },
            columns: [
                {data: 'sequence', name: 'sequence'},
                {data: 'display_name', name: 'display_name'},
                {data: 'active', name: 'active', orderable: false, searchable: false},
                {data: 'edit', name: 'edit', orderable: false, searchable: false}
            ],
            language: {
                url: '{{ asset('/admin/localization/nl/datatable.json') }}'
            }
        });

        oTable.on('row-reorder', function (e, diff, edit) {
            var order = [];

            for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                var rowData = oTable.row( diff[i].node ).data();

                order.push([
                    rowData.id,
                    diff[i].newData
                ]);
            }

            $.ajax({
                url: '{!! url('cms/module/reorder') !!}',
                type: 'POST',
                data: {
                    _token: '{!! csrf_token() !!}',
                    order: order
                }
            }).done(function(data) {
                oTable.draw(false);
            });
        });
    </script>
@endsection
