@extends('admin::layouts.admin')

@section('title', 'Nieuwsitem toevoegen')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::open(['url' => 'cms/news', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Vul de gegevens in</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('title', 'Titel') !!}
                            {!! Form::text('title', Input::old('title'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('posted_on', 'Datum') !!}
                            {!! Form::text('posted_on', \Carbon\Carbon::now()->format("d-m-Y"), ['class' => 'datepicker form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('intro', 'Intro') !!}
                            {!! Form::textarea('intro', Input::old('intro'), ['class' => 'ckeditor']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('text', 'Tekst') !!}
                            {!! Form::textarea('text', Input::old('text'), ['class' => 'ckeditor']) !!}
                        </div>
                        <hr />
                        <div class="form-group">
                            {!! Form::label('meta_title', 'Meta titel') !!}
                            {!! Form::text('meta_title', Input::old('meta_title'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('meta_description', 'Meta omschrijving') !!}
                            {!! Form::text('meta_description', Input::old('meta_description'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('meta_keywords', 'Meta steekwoorden') !!}
                            {!! Form::text('meta_keywords', Input::old('meta_keywords'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/news') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy'
        });
    </script>
@endsection