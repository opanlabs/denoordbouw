@extends('admin::layouts.admin')

@section('title', 'Nieuwe rol')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::open(['url' => 'cms/role', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Vul de gegevens in</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Naam') !!}
                            {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('display_name', 'Weergavenaam') !!}
                            {!! Form::text('display_name', Input::old('display_name'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Omschrijving') !!}
                            {!! Form::text('description', Input::old('description'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <div class="table-responsive">
                                {!! Form::label('permissions', 'Rechten') !!}
                                {!! Form::select('permissions[]', $permissions, null, ['id' => 'multi-select', 'multiple' => 'multiple']) !!}
                            </div>
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/role') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('/admin/libs/multiselect/jquery.multi-select.js') }}"></script>
    <script>
        $('#multi-select').multiSelect();
    </script>
@endsection