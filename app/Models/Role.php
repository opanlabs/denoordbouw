<?php

namespace App\Models;

use Zizaco\Entrust\EntrustRole;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Role extends EntrustRole implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'display_name', 'description', 'sequence'];

    /**
     * Get the permissions for the role.
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission');
    }
}