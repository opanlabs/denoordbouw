<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'name', 'code', 'label', 'sequence', 'active'];

    /**
     * The attribute sets that belong to the attribute.
     */
    public function sets()
    {
        return $this->belongsToMany('App\Models\AttributeSet');
    }

    /**
     * The products that belong to the category.
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot('value');
    }
}
