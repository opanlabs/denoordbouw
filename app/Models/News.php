<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogsActivity;
use Spatie\Activitylog\LogsActivityInterface;

class News extends Model implements LogsActivityInterface
{
    use Translatable, LogsActivity;

    /**
     * The attributes that are translated
     *
     * @var array
     */
    public $translatedAttributes = ['title', 'slug', 'posted_on', 'meta_title', 'meta_description', 'meta_keywords', 'text', 'intro'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'posted_on',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'text',
        'intro',
        'sequence',
        'active'
    ];

    /**
     * Get all the images.
     */
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    /**
     * Get the message that needs to be logged for the given event.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return "News({$this->id}) '{$this->title}' was created";
        }

        if ($eventName == 'updated')
        {
            return "News({$this->id}) '{$this->title}' was updated";
        }

        if ($eventName == 'deleted')
        {
            return "News({$this->id}) '{$this->title}' was deleted";
        }

        return '';
    }
}
