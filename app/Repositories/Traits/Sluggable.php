<?php

namespace App\Repositories\Traits;

trait Sluggable
{
    /**
     * Find resource by slug
     *
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->whereHas('translations', function($query) use ($slug)
        {
            $query->where('slug', '=', $slug);
        })->firstOrFail();
        $this->resetModel();
        return $this->parserResult($model);
    }
}