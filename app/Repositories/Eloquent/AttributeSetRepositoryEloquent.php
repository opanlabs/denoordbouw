<?php

namespace App\Repositories\Eloquent;


use App\Models\AttributeSet;
use App\Repositories\Contracts\AttributeSetRepository;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\SortableTrait;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

class AttributeSetRepositoryEloquent extends BaseRepository implements AttributeSetRepository
{
    use DatatalableTrait, SortableTrait, CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AttributeSet::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return Validator
     */
    public function validator()
    {
        return "App\\Repositories\\Validators\\AttributeSetValidator";
    }
}