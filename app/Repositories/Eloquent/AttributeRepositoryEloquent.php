<?php

namespace App\Repositories\Eloquent;

use App\Models\Attribute;
use App\Repositories\Contracts\AttributeRepository;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\SortableTrait;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

class AttributeRepositoryEloquent extends BaseRepository implements AttributeRepository
{
    use DatatalableTrait, SortableTrait, CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Attribute::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return Validator
     */
    public function validator()
    {
        return "App\\Repositories\\Validators\\AttributeValidator";
    }
}