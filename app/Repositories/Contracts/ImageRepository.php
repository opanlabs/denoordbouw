<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

interface ImageRepository extends RepositoryInterface
{
    public function reorder($order);

    public function activate($status, $id);
}