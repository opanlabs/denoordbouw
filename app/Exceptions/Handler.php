<?php

namespace App\Exceptions;

use App\Models\Page;
use App\Models\Block;
use App\Models\Setting;
use App\Repositories\Contracts\PageRepository;
use App\Repositories\Contracts\BlockRepository;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        if ($e instanceof NotFoundHttpException) {
			$settings = Setting::select('field', 'value')->get()->keyBy('field');
            $navigation = Page::whereActive(1)->whereNull('parent_id')->get();
			$data['settings']	=$settings;
			$data['navigation']	=$navigation;
			$data['statistic'] = Block::whereIn('id',array(5,12,13,14,15,16,17,18,19,20))->where('active',1)->get();
			foreach($data['navigation'] as $key=>$row)
			{
				
				if($row['id']==4){
					$data['navigation'][$key]['subpages']	=Page::whereActive(1)->where('parent_id',$row['id'])->get(); 
					
				}
				else
				{
					$data['navigation'][$key]['subpages']	= array();
				}
			}
			
			$data['subPagesNavigation']	=  Page::where('parent_id',4)->get();
		// echo"<pre>";print_r($data['subPagesNavigation']);die;
            return response()->view('errors.404', $data, 404);
        }

        return parent::render($request, $e);
    }
}
