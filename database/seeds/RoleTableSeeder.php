<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin = Role::create([
            'name' => 'superadmin',
            'display_name' => 'Superadmin',
            'description' => 'The superadmin has all the rights in the system.',
            'sequence' => 1
        ]);

        $admin = Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => '',
            'sequence' => 2
        ]);

        $editor = Role::create([
            'name' => 'editor',
            'display_name' => 'Editor',
            'description' => '',
            'sequence' => 3
        ]);

        // Load all permissions
        $allPermissions = Permission::all();

        // Permissions for all roles
        $defaultEditorPermissions = [
            'access-page', 'edit-page',
            'access-subpage', 'edit-subpage',
            'access-block', 'edit-block',
            'access-banner', 'edit-banner',
            'access-category', 'create-category', 'edit-category', 'delete-category',
            'access-product', 'create-product', 'edit-product', 'delete-product',
            'access-attribute', 'create-attribute', 'edit-attribute', 'delete-attribute',
            'access-attributeset', 'create-attributeset', 'edit-attributeset', 'delete-attributeset',
            'access-projectgroup', 'create-projectgroup', 'edit-projectgroup', 'delete-projectgroup',
            'access-project', 'create-project', 'edit-project', 'delete-project',
            'access-news', 'create-news', 'edit-news', 'delete-news',
            'access-setting', 'edit-setting',
            'reset-admin-password',
        ];

        // Set editor permissions
        foreach ($allPermissions as $permission) {
            if (in_array($permission->name, $defaultEditorPermissions)) {
                $editor->attachPermission($permission);
            }
        }

        // Admin has all permissions ass default
        foreach ($allPermissions as $permission) {
            $admin->attachPermission($permission);
        }
    }
}
