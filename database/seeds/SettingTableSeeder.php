<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Company settings
         */
        Setting::create([
            'field' => 'company',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'address',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'zip_code',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'city',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'email',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'phone',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'mobile',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'coc',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'vat',
            'value' => ''
        ]);

        /**
         * Google Analytics
         */
        Setting::create([
            'field' => 'ga_email',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'ga_site_id',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'ga_tracking_id',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'ga_client_id',
            'value' => ''
        ]);

        /**
         * Google Maps
         */
        Setting::create([
            'field' => 'gm_lat',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'gm_long',
            'value' => ''
        ]);

        /**
         * Social Media
         */
        Setting::create([
            'field' => 'facebook',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'twitter',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'instagram',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'linkedin',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'google',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'youtube',
            'value' => ''
        ]);

        /**
         * Messages
         */
        Setting::create([
            'field' => 'message_404',
            'value' => ''
        ]);

        Setting::create([
            'field' => 'message_503',
            'value' => ''
        ]);
    }
}
