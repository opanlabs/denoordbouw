<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sequence');
            $table->tinyInteger('active')->index();
            $table->timestamps();
        });

        Schema::create('block_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('block_id')->unsigned();
            $table->string('locale')->index();

            $table->string('title');
            $table->string('slug')->index();
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('meta_keywords');
            $table->text('text');
            $table->string('button_link')->nullable();
            $table->string('button_text')->nullable();

            $table->unique(['block_id', 'locale']);
            $table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('block_translations');
        Schema::drop('blocks');
    }
}
