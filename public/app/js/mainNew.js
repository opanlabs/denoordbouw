
function getcaptcha(){
	 grecaptcha.render('gocaptcha', {
				'sitekey' : '6LfQaiAUAAAAAEEZG4fSQ6a26aPmPnvD2AC0M_3R'
			});
	
}

$( document ).ready(function() {
	
   
	$('.testimoni-slide-column').slick({
	  infinite: true,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  nextArrow: '<i class="fa fa-angle-right right" aria-hidden="true"></i>',
  	  prevArrow: '<i class="fa fa-angle-left left" aria-hidden="true"></i>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        infinite: true,
	        nextArrow: false,
	        prevtArrow: false,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 1,
	        infinite: true,
	        slidesToScroll: 1,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        infinite: true,
	        slidesToScroll: 1,
	        dots: true
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	 $('.slider-for').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,	  
	  fade: true,
	  nextArrow: '<i class="fa fa-angle-right right" aria-hidden="true"></i>',
  	  prevArrow: '<i class="fa fa-angle-left left" aria-hidden="true"></i>',
	  asNavFor: '.slider-nav'
	});
	$('.slider-nav').slick({
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  asNavFor: '.slider-for',
	  dots: false,
	  arrows: false,
	  centerMode: true,
	  focusOnSelect: true
	});
	// if(page==1 || page==8)
	if( page==8)
	{
		 setTimeout(function(){
			
			 grecaptcha.render('gocaptcha', {
				'sitekey' : '6LfQaiAUAAAAAEEZG4fSQ6a26aPmPnvD2AC0M_3R'
			});
			
		 },3500);
	}
	$(".contact-form").submit(function(e) {
		 $('#info-status').removeClass('bg-danger');
		 
		var msgRequired =' can not be empty';
		var msgExtNotValid =' does not match format';
		// var msgExtNotValid =' does not match format';
		var name = $('.name').val();
		// var phone = $('.phones').val();
		var email = $('.email').val();
		var subject = $('.subject').val();
	
		var emailFilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		var phoneFilter = /[^0-9\.]/g;
		var valid=true;
		 $('#info-status').html('');
		var msg ='<div class="alert alert-danger"><ul>';
        
		
		if(name=="")
		{
			msg+='<li>Vul je naam in.</li>';
			
			valid=false;
		}
		
		
		 if(email=="")
		{
			msg+='<li>Vul je e-mailadres in.</li>';
			
			valid=false;
		}
		else
		{
			
			if(!email.match(emailFilter))
			  {
					valid =false;
					alert('email2');
					msg+='<li>Vul een geldig e-mailadres in.</li>';
			  }
			 
		 }
		if(subject=="")
		{
			msg+='<li> Vul je subject in. </li>';
			
			valid=false;
		}
		if(grecaptcha.getResponse() == "")
		{
			msg+='<li>Verifieer dat u geen robot bent.</li>';
			valid=false;
		}
		if(valid==false)
		{
			 $('#info-status').html(msg+'</ul></div>');
			  $('#info-status').addClass('bg-danger');
			 	return false;
		}
		// return false;
        
    });
	function createAlert(msg,type){
        var title = '';
        if(type == 'success'){
            title = 'Success';
        } else {
            title = 'Failed';
        }

        return '<div id="alert" class="alert alert-'+ type +' alert-dismissible fade in" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' +
            '<strong>'+ title +'!</strong> ' + msg +
            '</div>';
    }
});